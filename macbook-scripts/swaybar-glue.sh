#!/bin/sh -e
# glue for swaybar data

if [ $1 = "cron" ]
then

# gmail, åvalla, weechat, temperature
set $(cat /run/cron-to-swaybar)
unr="$1"
unr2="$2"

if [ $unr -eq "0" ] || [ -z "$unr" ]
then
  unr=$(curl -s \
  --url "imaps://mail.REDACTED:993" \
  --user "info@REDACTED:password"  -X 'STATUS INBOX (UNSEEN)' \
  2>&1 | sed 's/[^0-9]*//g' )
fi

if [ $unr2 -eq "0" ] || [ -z "$unr2" ]
then
  unr2=$(curl -s \
  --url "imaps://imap.gmail.com:993" \
  --user "REDACTED:password"  -X 'STATUS INBOX (UNSEEN)' \
  2>&1 | sed 's/[^0-9]*//g' )
fi

tem=$(curl -fL http://www.temperatur.nu/termo/REDACTED/temp.txt)

set $unr $unr2 $3 $tem
echo $@ > /run/cron-to-swaybar




elif [ $1 = "weechat" ]
then

set $(cat /run/cron-to-swaybar)

n=$(expr $3 + 1)

set $1 $2 $n $4
echo $@ > /run/cron-to-swaybar



elif [ $1 = "clear" ]
then

set $(cat /run/cron-to-swaybar)
set 0 0 0 $4
echo $@ > /run/cron-to-swaybar

fi


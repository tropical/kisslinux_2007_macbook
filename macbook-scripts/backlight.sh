#!/bin/sh

case "$1" in                                                                    
    -inc)                            
        actual=$(cat /sys/class/backlight/acpi_video0/brightness)
        value=$((actual + 1))   
        if [ $value -ge 9 ]; then
            value=14
        fi
        echo -n $value > /sys/class/backlight/acpi_video0/brightness
        ;;                          
    -dec)                           
        actual=$(cat /sys/class/backlight/acpi_video0/brightness)
        value=$((actual - 1))   
        if [ $value -le 0 ]; then
            value=0    
        fi
        if [ $value -ge 9 ]; then
            value=8
        fi
        echo -n $value > /sys/class/backlight/acpi_video0/brightness
        ;;                          
    *)                              
        echo "-inc or -dec"
        exit 1                      
        ;;                          
esac

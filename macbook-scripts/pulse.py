#!/bin/python
import subprocess
import time
import signal
import sys

def signal_handler(sig, frame):
    f = open("/sys/class/backlight/acpi_video0/brightness", "w")
    f.write("15")
    f.close()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


while True:
    for n in range(2,15):
        time.sleep(0.02)
        f = open("/sys/class/backlight/acpi_video0/brightness", "w")
        f.write(str(n))
        f.close()
    
    for n in range(15, 1, -1):
        time.sleep(0.02)
        f = open("/sys/class/backlight/acpi_video0/brightness", "w")
        f.write(str(n))
        f.close()

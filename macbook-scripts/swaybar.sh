#!/bin/sh

vcc=$(cat /sys/class/power_supply/BAT0/voltage_now)
vcc=$(echo -n "scale=2; $vcc / 1000000" | bc -l)V
dat=$(date +'%A %Y-%m-%d %H:%M')

# gmail, åvalla, weechat, temperature
set -- $(cat /run/cron-to-swaybar)
unr="$1"
unr2="$2"
not="$3"
tem="$4"°

if [ $not -gt 0 ]
then
  if [ $not -eq 1 ]
  then
    not="You've got a message on IRC!    "
  else
    not="You've got $not messages on IRC!    "
  fi
else
  not=""
fi

if [ $unr -gt 0 ]
then
  if [ $unr -eq 1 ]
  then
    unr="You've got mail! (Åvalla)    "
  else
    unr="You've got $unr new emails! (Åvalla)    "
  fi
else
  unr=""
fi

if [ $unr2 -gt 0 ]
then
  if [ $unr2 -eq 1 ]
  then
    unr2="You've got mail! (Gmail)    "
  else
    unr2="You've got $unr2 new emails! (Gmail)    "
  fi
else
  unr2=""
fi

echo "$tem    $not$unr$unr2$vcc    $dat"
